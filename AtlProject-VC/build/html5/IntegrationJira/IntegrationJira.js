angular
    .module('IntegrationJiraApp', ['snk'])
    .controller('IntegrationJiraController', ['SkApplication', 'i18n', 'ObjectUtils', 'MGEParameters', 'AngularUtil', 'StringUtils', 'ServiceProxy', 'MessageUtils', 'SanPopup',
        function(SkApplication, i18n, ObjectUtils, MGEParameters, AngularUtil, StringUtils, ServiceProxy, MessageUtils, SanPopup) {
            var self = this;

            var _dsIntegraJira;
            var _dynaformIntegraJira;
            var _personalizedFilter;
            var _navigator;

            //Dynaform Interceptors
            self.onDynaformLoaded = onDynaformLoaded;
            self.customTabsLoader = customTabsLoader;
            self.interceptNavigator = interceptNavigator;
            self.interceptPersonalizedFilter = interceptPersonalizedFilter;

            <%=interceptors.interceptorsDynaformHeaders%>            

            //Dataset Interceptors
            ObjectUtils.implements(self, IDataSetObserver);            

            <%=interceptors.interceptorsDatasetHeaders%>

            //Dynaform intercepttors implementation
            ObjectUtils.implements(self, IDynaformInterceptor);
            
            function onDynaformLoaded(dynaform, dataset) {
                _dynaformIntegraJira = dynaform;
                _dsIntegraJira = dataset;

                _dsIntegraJira.addObserver(self);

                <%=navigatorConfig%>
            }
            
            function customTabsLoader(entityName) {                
                if (entityName == 'IntegraJira') {
                    var customTabs = [];
                    //{customTabs placeholder}
                    return customTabs;
                }
            }            

            function interceptPersonalizedFilter(personalizedFilter, dataset) {
                _personalizedFilter = personalizedFilter;
            }

            function interceptNavigator(navigator, dynaform) {
                _navigator = navigator;
            }

            <%=interceptors.interceptorsDynaform%>

            //Dataset interceptors implementation
            <%=interceptors.interceptorsDataset%>
            
            //{popup callers placeholder}           
        }
    ]);